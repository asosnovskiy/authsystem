var mongoose = require('libs/mongoose');

var schema = new mongoose.Schema({
    username: {
        type: String,
        required: true,
        unique: true
    },
    password: {
        type: String,
        required: true
    },
    data: {
        type: {},
        default: {}
    }
});

module.exports = mongoose.model('Account', schema);