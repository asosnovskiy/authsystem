var nconf = require('nconf');
var path = require('path');

var fileName = "config.json";

nconf.argv()
    .env()
    .file({file: path.join(__dirname, fileName)});

module.exports = nconf;